from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


app = Flask(__name__)
Base = declarative_base()

class CarTiozao(Base):
    __tablename__= 'CarTiozao'
    id = Column(Integer, primary_key=True, autoincrement=True)
    carro = Column(String(300), nullable=False)

    def __init__(self, id=None, carro=None):
        self.id = id
        self.carro = carro

    def __repr__(self):
        return f'{self.carro}'

def set_routes(session):
    @app.route('/', methods=['GET'])
    def index():
        return render_template('index.html', all=session.query(CarTiozao).all())

    @app.route('/listing', methods=['POST'])
    def listing():
        req = request.form['carro']
        session.add(CarTiozao(carro=req))
        session.commit()
        return redirect("/")

    @app.route("/update", methods=["POST"])
    def update():
        old_title = request.form['old_title']
        new_title = request.form['new_title']
        item = session.query(CarTiozao).filter_by(carro=old_title).first()
        session.delete(item)
        session.add(CarTiozao(carro=new_title))
        session.commit()
        return redirect("/")

    @app.route("/delete", methods=["POST"])
    def delete():
        delete_title = request.form['delete_title']
        item = session.query(CarTiozao).filter_by(carro=delete_title).first()
        session.delete(item)
        session.commit()
        return redirect("/")


if __name__ == '__main__':
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///carros.db'
    engine = create_engine('sqlite:///carros.db')
    Session = sessionmaker(bind = engine)
    session_ = Session()
    Base.metadata.create_all(engine)
    set_routes(session_)
    app.run(debug = True, host='0.0.0.0')
